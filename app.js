const { accountSid, authToken, twilioNumber, myNumber, twilioWhatsApp, myWhatsApp, fbPageId, fbUserId } = require('./constants')
const bodyParser = require('body-parser')
const client = require('twilio')(accountSid, authToken)
const app = require('express')()

app.use(bodyParser.urlencoded({ extended: false }));

// SMS -> WhatsApp & Facebook
app.post('/sms', (req, res) => {
    const message = req.body.Body
    client.messages // sends message to whatsapp
        .create({
            from: twilioWhatsApp,
            body: message,
            to: myWhatsApp
        })
        .then(msg => console.log(msg))
    
    client.messages // sends message to facebook page
        .create({
            from: fbPageId,
            body: message,
            to: fbUserId
        })
        .then(msg => console.log(msg))
})

// WhatsApp -> SMS & Facebook
app.post('/whatsapp', (req, res) => {
    const message = req.body.Body
    client.messages
        .create({
            from: twilioNumber,
            body: message,
            to: myNumber
        })
        .then(msg => console.log(msg))

    client.messages
        .create({
            from: fbPageId,
            body: message,
            to: fbUserId
        })
        .then(msg => console.log(msg))
})
// Facebook -> SMS
app.post('/facebook', (req, res) => {
    console.log(req.body)
    const message = req.body.Body
    client.messages
        .create({
            from: twilioNumber,
            body: message,
            to: myNumber
        })
        .then(msg => console.log(msg))
})

// Facebook -> SMS
app.post('/facebook', (req, res) => {
    const message = req.body.Body
    console.log(req.body)
    client.messages
        .create({
            from: twilioNumber,
            body: message,
            to: myNumber
        })
        .then(msg => console.log(msg))
})


const port = 3000
app.listen(port, () => {
    console.log(`port up on ${port}`)
})