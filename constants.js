require('dotenv').config()

const accountSid = process.env.ACCOUNT_SID
const authToken = process.env.AUTH_TOKEN
const twilioNumber = '+17622432863'
const myNumber = '+16822391122'
const twilioWhatsApp = 'whatsapp:+14155238886'
const myWhatsApp = 'whatsapp:+16822391122'
const fbPageId = 'messenger:105637485466052'
const fbUserId = 'messenger:5138643169515794'

module.exports = {
    accountSid,
    authToken,
    twilioNumber,
    myNumber,
    twilioWhatsApp,
    myWhatsApp,
    fbPageId,
    fbUserId,
}